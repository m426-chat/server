FROM node:fermium AS builder
WORKDIR /app
COPY . .
RUN yarn install --frozen-lockfile \
 && yarn build \
 && rm -rf node_modules/
CMD yarn start:dist
