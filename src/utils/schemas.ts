import { Schema } from 'jsonschema';

export const registerRequestSchema: Schema = {
  type: 'object',
  required: ['email', 'username', 'password'],
  properties: {
    email: { type: 'string' },
    username: { type: 'string' },
    password: { type: 'string' },
  },
};
export const settingsRequestSchema: Schema = {
  type: 'object',
  required: ['email', 'username'],
  properties: {
    email: { type: 'string' },
    username: { type: 'string' },
  },
};
export const newChatSchema: Schema = {
  type: 'object',
  required: ['username'],
  properties: {
    username: { type: 'string' },
    chatName: { type: 'string' },
  },
};
