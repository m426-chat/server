import { Socket } from 'socket.io';
import { ChatEntity } from '../db/entities/chat.entity';
import { MessageEntity } from '../db/entities/message.entity';
import { UserEntity } from '../db/entities/user.entity';

export type PartialBy<T, K extends keyof T> = Omit<T, K> & Partial<Pick<T, K>>;

export type PartialMessage = PartialBy<MessageEntity, 'id'>;
export type PartialChat = PartialBy<ChatEntity, 'id'>;
export type PartialUser = PartialBy<UserEntity, 'id'>;

export type AuthSocket = Socket & { tokenPayload?: JWTPayload };

export interface JWTPayload {
  id: number;
  email: string;
  username: string;
}
