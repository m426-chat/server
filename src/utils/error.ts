import { Request, Response, NextFunction } from 'express';

export const errorMiddleware: any = () => (err: HttpError, _req: Request, res: Response, _next: NextFunction) => {
  const status = err.status || 400;
  res.status(status).send({
    err: err.message,
  });
};

export class HttpError extends Error {
  get message(): string {
    return this.msg;
  }
  get status(): number {
    return this.code;
  }
  constructor(private code: number, private msg: string) {
    super(msg);
  }
}
