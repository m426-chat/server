import { NextFunction, Request } from 'express';
import JWT from 'jsonwebtoken';
import { HttpError } from './error';
import { JWTPayload } from './types';

export namespace Auth {
  const defaultSecret = 'secret';
  export function createToken(payload: JWTPayload): string {
    return JWT.sign(payload, process.env.SECRET || defaultSecret);
  }
  export function verifyToken(token?: string): JWTPayload {
    token ||= '';
    return JWT.verify(token, process.env.SECRET || defaultSecret) as JWTPayload;
  }
}

export const authMiddleware: any = () => (req: any, _res: Response, next: NextFunction) => {
  try {
    void Auth.verifyToken(req.token);
    next();
  } catch (err) {
    next(new HttpError(401, err.message));
  }
};
