import { Server as SocketServer } from 'socket.io';
import { DbProvider } from './db/database';
import { MessageEntity } from './db/entities/message.entity';
import { AuthSocket, PartialMessage } from './utils/types';

export const socketConnectionHandler = (io: SocketServer, socket: AuthSocket) => {
  socket.send('You are connected');
  if (!socket.tokenPayload) socket.send('You are not authenticated');
  else {
    let activeChatRoom: { roomId: number; roomName: string } | undefined = undefined;
    socket.send(`You are authenticated as ${socket.tokenPayload.username} (id: ${socket.tokenPayload.id})`);

    socket
      .on('chat:connect', (roomId: number) => {
        // Leave other chat rooms
        if (activeChatRoom) socket.leave(activeChatRoom.roomName);

        const roomName = `chat/${roomId}`;
        socket.join(roomName);
        socket.send(`Connected you to room '${roomName}'`);
        activeChatRoom = { roomId, roomName };
      })
      .on('chat:message:send', async (newMessage: string) => {
        if (activeChatRoom) {
          console.log(`New message from ${socket.tokenPayload?.username}: ${newMessage}`);
          const newPartialMessage = await sendMessage(newMessage, socket.tokenPayload!.id, activeChatRoom.roomId);

          io.to(activeChatRoom.roomName).emit('chat:message:recieve', {
            ...newPartialMessage,
            author: newPartialMessage.author.username,
          });
        } else {
          socket.send('You are not connected to any chats');
        }
      });
  }
};

async function sendMessage(message: string, userId: number, chatId: number): Promise<MessageEntity> {
  const dbProvider = await DbProvider.getInstance();

  const chat = await dbProvider.chatRepo.findOne({ where: { id: chatId } });
  if (!chat) throw new Error('Chat reference not found in database');

  const author = await dbProvider.userRepo.findOne({ where: { id: userId } });
  if (!author) throw new Error('Author reference not found in the database');

  const newMessageEntity: PartialMessage = {
    content: message,
    timestamp: new Date(),
    chat: chat,
    author: author,
  };

  console.log('New Message:', newMessageEntity.chat);

  return await dbProvider.messageRepo.save(newMessageEntity);
}
