import bodyParser from 'body-parser';
import { Router } from 'express';
import { DbProvider } from '../db/database';
import { HttpError } from '../utils/error';

export const checkRouter = Router();

checkRouter.use(bodyParser.json());

checkRouter.post('/email', async (req, res, next) => {
  try {
    const requestedEmail = req.body.email;
    if (!requestedEmail) throw new Error('No email was provided');

    const dbProvider = await DbProvider.getInstance();
    const foundUser = await dbProvider.userRepo.findOne({ where: { email: requestedEmail } });
    if (foundUser) throw new Error('This email is already registered by another user');

    res.status(200).send({ message: 'This email is still available' });
  } catch (err) {
    next(new HttpError(400, err.message));
  }
});

checkRouter.post('/username', async (req, res, next) => {
  try {
    const requestedUsername = req.body.username;
    if (!requestedUsername) throw new Error('No username was provided');

    const dbProvider = await DbProvider.getInstance();
    const foundUser = await dbProvider.userRepo.findOne({ where: { username: requestedUsername } });
    if (foundUser) throw new Error('This username is already registered');

    res.status(200).send({ message: 'This username is still available' });
  } catch (err) {
    next(new HttpError(400, err.message));
  }
});
