import express from 'express';
import bodyParser from 'body-parser';
import hasha from 'hasha';
import { DbProvider } from '../db/database';
import { Auth } from '../utils/auth';
import { HttpError } from '../utils/error';

export const loginRouter = express.Router();

loginRouter.use(bodyParser.json());

loginRouter.post('/', async (req, res, next) => {
  const body = req.body;

  try {
    const dbProvider = await DbProvider.getInstance();
    const foundUser = await dbProvider.userRepo.findOne({
      where: [{ email: body.email }, { username: body.username }],
    });

    if (!foundUser) throw new Error("This user doesn't exist");
    const givenPassword = hasha(body.password);

    if (foundUser.password === givenPassword) {
      res.status(200).send({
        message: 'Recieved valid credentials',
        token: Auth.createToken({
          id: foundUser.id,
          email: foundUser.email,
          username: foundUser.username,
        }),
      });
    } else throw new Error("This user doesn't exist");
  } catch (err) {
    next(new HttpError(400, err.message));
  }
});
