import bearerToken from 'express-bearer-token';
import { Router } from 'express';
import { DbProvider } from '../db/database';
import { Auth, authMiddleware } from '../utils/auth';
import { HttpError } from '../utils/error';
import bodyParser from 'body-parser';
import { validate } from 'jsonschema';
import { newChatSchema } from '../utils/schemas';
import { PartialBy } from '../utils/types';
import { ChatEntity } from '../db/entities/chat.entity';

export const chatRouter = Router();

chatRouter.use(bearerToken());
chatRouter.use(bodyParser.json());
// Authentication
chatRouter.use(authMiddleware());

chatRouter.get('/list', async (req, res, next) => {
  const decoded = Auth.verifyToken(req.token);
  const dbProvider = await DbProvider.getInstance();

  const user = await dbProvider.userRepo.findOne({
    where: { username: decoded.username },
    relations: ['chats', 'chats.messages'],
  });

  if (!user) next(new HttpError(500, `The user with username '${decoded.username}'`));
  const chats = user!.chats;
  const chatsWithLastMessage = chats.map((chat) => {
    const lastMessage = chat.messages[chat.messages.length - 1];
    return { ...chat, lastMessage };
  });
  res.status(200).send({ chats: chatsWithLastMessage });
});

chatRouter.post('/', async (req, res, next) => {
  try {
    const decoded = await Auth.verifyToken(req.token);
    const { body } = req;
    validate(body, newChatSchema, { throwError: true });

    const dbProvider = await DbProvider.getInstance();
    const requester = await dbProvider.userRepo.findOne({
      where: { username: decoded.username },
    });
    const target = await dbProvider.userRepo.findOne({
      where: { username: body.username },
    });

    if (!requester) next(new HttpError(500, 'Could not resolve a user from the provided token'));
    if (!target) next(new HttpError(400, `The target user '${body.username}' could not be found`));

    const newChat: PartialBy<ChatEntity, 'id'> = {
      title: target!.username,
      users: [requester!, target!],
      messages: [],
    };
    await dbProvider.chatRepo.save(newChat);

    res.status(201).send({
      message: `Created new chat with user '${target?.username}'`,
    });
  } catch (err) {
    next(new HttpError(400, err.message));
  }
});

chatRouter.get('/:id', async (req, res, next) => {
  const decoded = Auth.verifyToken(req.token);
  const dbProvider = await DbProvider.getInstance();

  const chat = await dbProvider.chatRepo.findOne({
    where: { id: req.params.id },
    relations: ['messages', 'messages.author'],
  });

  if (!chat) next(new HttpError(500, `The chat with id '${req.params.id}'`));
  const messages = chat!.messages;
  const messagesWithAuthorTimestampstring = messages.map((message) => {
    const timestamp = message.timestamp.getTime();
    return { ...message, author: message.author.username, timestamp };
  });
  res.status(200).send({ messages: messagesWithAuthorTimestampstring });
});
