import { Router } from 'express';
import { join } from 'path';

export const indexRouter = Router();

indexRouter.get('/', (_req, res) => {
  res.status(200).render(join(__dirname, '../pages/index.ejs'));
});
