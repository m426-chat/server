import { Router } from 'express';
import bearerToken from 'express-bearer-token';
import { DbProvider } from '../db/database';
import { Auth, authMiddleware } from '../utils/auth';
import { HttpError } from '../utils/error';
import bodyParser from 'body-parser';
import { validate } from 'jsonschema';
import { newChatSchema } from '../utils/schemas';
import hasha from 'hasha';
import { settingsRequestSchema } from '../utils/schemas';
import { PartialBy } from '../utils/types';
import { UserEntity } from '../db/entities/user.entity';
export const settingsRouter = Router();

settingsRouter.use(bearerToken());
settingsRouter.use(bodyParser.json());
// Authentication
settingsRouter.use(authMiddleware());

settingsRouter.get('/', async (req, res, next) => {
  const decoded = Auth.verifyToken(req.token);
  const dbProvider = await DbProvider.getInstance();

  const user = await dbProvider.userRepo.findOne({
    where: { username: decoded.username },
  });
  if (!user) next(new HttpError(500, `The user with username '${decoded.username}'`));
  res.status(200).send({ user: user });
});

settingsRouter.post('/', async (req, res, next) => {
  const decoded = Auth.verifyToken(req.token);
  const dbProvider = await DbProvider.getInstance();

  const user = await dbProvider.userRepo.findOne({
    where: { username: decoded.username },
  });
  if (!user) next(new HttpError(500, `The user with username '${decoded.username}'`));
  const body = req.body;

  validate(body, settingsRequestSchema, { throwError: true });

  user!.username = body.username;
  user!.email = body.email;
  user!.description = body.description;

  await dbProvider.userRepo.save(user!);

  res
    .status(200)
    .send({ user: user, token: Auth.createToken({ email: user!.email, username: user!.username, id: user!.id }) });
});
