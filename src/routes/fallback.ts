import { Router } from 'express';
import { join } from 'path';

export const fallbackRouter = Router();

fallbackRouter.get('*', (req, res, next) => {
  res.status(200).render(join(__dirname, '../pages/fallback.ejs'), { path: req.path });
});
