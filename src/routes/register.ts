import express from 'express';
import bodyParser from 'body-parser';
import hasha from 'hasha';
import { validate } from 'jsonschema';
import { registerRequestSchema } from '../utils/schemas';
import { DbProvider } from '../db/database';
import { PartialBy } from '../utils/types';
import { UserEntity } from '../db/entities/user.entity';
import { Auth } from '../utils/auth';
import { HttpError } from '../utils/error';

export const registerRouter = express.Router();

registerRouter.use(bodyParser.json());

registerRouter.post('/', async (req, res, next) => {
  const body = req.body;
  try {
    validate(body, registerRequestSchema, { throwError: true });

    const dbProvider = await DbProvider.getInstance();

    const newUser: PartialBy<UserEntity, 'id'> = {
      username: body.username,
      email: body.email,
      password: hasha(body.password),
      description: body.description,
      chats: [],
      messages: [],
    };
    const savedUser = await dbProvider.userRepo.save(newUser);

    res.status(200).send({
      message: 'Recieved valid inputs and saved new user',
      token: Auth.createToken({
        id: savedUser.id,
        email: savedUser.email,
        username: savedUser.username,
      }),
    });
  } catch (err) {
    next(new HttpError(400, err.message));
  }
});
