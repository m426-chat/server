import 'reflect-metadata';
import cors from 'cors';
import express from 'express';
import { Auth } from './utils/auth';
import { socketConnectionHandler } from './socket';
import { Server as SocketServer } from 'socket.io';
import { AuthSocket } from './utils/types';
import { errorMiddleware } from './utils/error';
import {
  checkRouter,
  indexRouter,
  loginRouter,
  registerRouter,
  chatRouter,
  settingsRouter,
  fallbackRouter,
} from './routes';

(async () => {
  await import('dotenv').then((dotenv) => dotenv.config());

  const app = express();
  const server = await import('http').then((http) => http.createServer(app));
  const io = require('socket.io')(server, { cors: { origin: '*' } }) as SocketServer;

  io.use(async (socket: AuthSocket, next) => {
    try {
      const token = socket.handshake.query.token as string | undefined;
      const decoded = Auth.verifyToken(token);
      socket.tokenPayload = decoded;
      next();
    } catch (err) {
      next(new Error('Invalid token'));
    }
  });
  io.on('connection', (socket: AuthSocket) => socketConnectionHandler(io, socket));

  app.set('view-engine', 'ejs');
  app.use(cors());

  app.use('/', indexRouter);
  app.use('/login', loginRouter);
  app.use('/register', registerRouter);
  app.use('/check', checkRouter);
  app.use('/chat', chatRouter);
  app.use('/settings', settingsRouter);
  app.use('*', fallbackRouter);

  app.use(errorMiddleware());

  const port = process.env.PORT || 3000;
  server.listen(port, () => console.log(`Server is listening on http://localhost:${port}`));
})();
