import { Connection, createConnection, Repository } from 'typeorm';
import { UserEntity } from './entities/user.entity';
import { MessageEntity } from './entities/message.entity';
import { ChatEntity } from './entities/chat.entity';

export class DbProvider {
  private connection: Connection;
  private static instance?: DbProvider;

  userRepo: Repository<UserEntity>;
  messageRepo: Repository<MessageEntity>;
  chatRepo: Repository<ChatEntity>;

  static async getInstance(): Promise<DbProvider> {
    if (DbProvider.instance) return DbProvider.instance;
    else {
      const dbProvider = new DbProvider();
      dbProvider.connection = await createConnection();

      dbProvider.chatRepo = dbProvider.connection.getRepository(ChatEntity);
      dbProvider.userRepo = dbProvider.connection.getRepository(UserEntity);
      dbProvider.messageRepo = dbProvider.connection.getRepository(MessageEntity);

      if (process.env.CLEAR_DB) {
        console.log('Clearing database');

        dbProvider.chatRepo.clear();
        dbProvider.userRepo.clear();
        dbProvider.messageRepo.clear();
      }

      DbProvider.instance = dbProvider;
      return dbProvider;
    }
  }
}
