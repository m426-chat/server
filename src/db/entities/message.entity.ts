import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { ChatEntity } from './chat.entity';
import { UserEntity } from './user.entity';
@Entity()
export class MessageEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  timestamp: Date;

  @Column({ nullable: false })
  content: string;

  @ManyToOne(() => ChatEntity, (chat) => chat.messages, { nullable: false })
  chat: ChatEntity;

  @ManyToOne(() => UserEntity, (user) => user.messages, { nullable: false })
  author: UserEntity;
}
