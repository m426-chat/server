import { Entity, PrimaryGeneratedColumn, Column, ManyToMany, JoinTable, OneToMany } from 'typeorm';
import { ChatEntity } from './chat.entity';
import { MessageEntity } from './message.entity';

@Entity()
export class UserEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  username: string;

  @Column()
  password: string;

  @Column({ unique: true })
  email: string;

  @Column({ default: '' })
  description: string;

  @ManyToMany(() => ChatEntity, (chat) => chat.users)
  @JoinTable()
  chats: ChatEntity[];

  @OneToMany(() => MessageEntity, (message) => message.author, { nullable: false })
  messages: MessageEntity[];
}
