import { Entity, PrimaryGeneratedColumn, Column, ManyToMany, ManyToOne, OneToMany } from 'typeorm';
import { MessageEntity } from './message.entity';
import { UserEntity } from './user.entity';

@Entity()
export class ChatEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  title: string;

  @ManyToMany(() => UserEntity, { nullable: false })
  users: UserEntity[];

  @OneToMany(() => MessageEntity, (message) => message.chat, { nullable: false })
  messages: MessageEntity[];
}
